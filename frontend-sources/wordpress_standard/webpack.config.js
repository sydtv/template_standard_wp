const
    path = require('path'),
    extPath = path.resolve(__dirname, '../../wp-content/themes/', path.basename('template_standard_wp')),
    distPath = path.join(extPath, 'assets'),
    { CleanWebpackPlugin } = require('clean-webpack-plugin'),
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    MiniCssExtractPlugin = require('mini-css-extract-plugin'),
    WriteFilePlugin = require('write-file-webpack-plugin');

module.exports = (env, argv) => {
    return {
        plugins: [
            new CleanWebpackPlugin({
                cleanStaleWebpackAssets: false
            }),
            new MiniCssExtractPlugin({
                filename: 'css/main.css'
            }),
            new CopyWebpackPlugin([
                {
                    from: path.resolve('./images/'),
                    to: path.join(distPath, 'images')
                }
            ]),
            new WriteFilePlugin()
        ],
        entry: ['./js/main.js', './scss/main.scss'],
        output: {
            path: distPath,
            filename: 'js/main.js'
        },
        devtool: argv.mode == 'development' ? 'source-map' : 'none',
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                    }
                },
                {
                    test: /\.(sass|scss)$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader
                        },
                        {
                            loader: "css-loader", // translates CSS into CommonJS
                            options: {
                                sourceMap: argv.mode === 'development'
                            }
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                sourceMap: argv.mode === 'development',
                                config: {
                                    path: 'postcss.config.js'
                                }
                            }
                        },
                        {
                            loader: "resolve-url-loader" // resolves url() definitions
                        },
                        {
                            loader: "sass-loader", // compiles SASS to CSS
                            options: {
                                sourceMap: true // must be active to on for resolve-url-loader
                            }
                        }
                    ]
                },
                {
                    test: require.resolve('jquery'),
                    loader: 'expose-loader',
                    options: {
                        exposes: ['$', 'jQuery']
                    }
                }
            ]
        },
        watch: argv.mode === 'development',
        watchOptions: {
            ignored: /node_modules/
        },
        devServer: {
            https: false,
            port: 3000,
            proxy: {
                '**': {
                    target: 'https://webstobe.local:443',
                    secure: false,
                    changeOrigin: true
                }
            }
        }
    }
};