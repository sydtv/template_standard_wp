/**
 * Test
 *
 * class to test setup
 *
 */

export default class Test {

    constructor() {
        this.initialized = false
    }

    static initialize() {
        if (this.initialized) {
            return;
        }

        console.log('test in class');

        this.initialized = true;
    }

    static destroy() {
        if (!this.initialized) {
            return;
        }

        this.initialized = false;
    }
}
